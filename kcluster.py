import math

class KCluster:
    def __init__(self, seed_x, seed_y):
        self.s_x = None
        self.s_y = None
        self.s_x = seed_x
        self.s_y = seed_y
        self.points = []

    def AddPoint(self, p):
        self.points.append(p)

    def ClearPoints(self):
        self.points = []

    def AddClusterAttribute(self, i, c):
        updateMap = {}
        for p in self.points:
            updateMap[p.id()] = {i: c}
        return updateMap

    def UpdateSeed(self):
        sum_x = 0.0
        sum_y = 0.0

        for p in self.points:
            geom = p.geometry()
            sum_x += geom.asPoint().x()
            sum_y += geom.asPoint().y()
        if sum_x == 0 and sum_y == 0:
            print('Sums are 0: Didn\'t update seed')
        else:
            avg_x = sum_x / len(self.points)
            avg_y = sum_y / len(self.points)
            self.s_x = avg_x
            self.s_y = avg_y

    def Print(self):
        print("Seed ({0}, {1})".format(self.s_x, self.s_y))
        for p in self.points:
            geom = p.geometry()
            print("\tPoint: ({0}, {1})".format(geom.asPoint().x(), geom.asPoint().y()))
        print # newline for sepration

    def PrintSeed(self):
        print("Cluster center ({0}, {1})".format(self.s_x, self.s_y))

    def Distance(self, p):
        geom = p.geometry()
        dist = math.sqrt( math.pow( (geom.asPoint().x() - self.s_x), 2) +\
                          math.pow( (geom.asPoint().y() - self.s_y), 2) )
        return dist
