# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ClusterAnalysis
                                 A QGIS plugin
 Plugin to assign clusters in vector data
                             -------------------
        begin                : 2016-04-28
        copyright            : (C) 2016 by Tucker
        email                : chaptuck@isu.edu
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load ClusterAnalysis class from file ClusterAnalysis.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .cluster_analysis import ClusterAnalysis
    return ClusterAnalysis(iface)
