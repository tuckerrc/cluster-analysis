# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ClusterAnalysis
                                 A QGIS plugin
 Plugin to assign clusters in vector data
                              -------------------
        begin                : 2016-04-28
        git sha              : $Format:%H$
        copyright            : (C) 2016 by Tucker
        email                : chaptuck@isu.edu
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import QSettings, QTranslator, qVersion, QCoreApplication, QVariant
from PyQt4.QtGui import QAction, QIcon, QProgressBar
# Initialize Qt resources from file resources.py
import resources
# Import the code for the dialog
from cluster_analysis_dialog import ClusterAnalysisDialog
import os.path
import random
import sys

from qgis.core import QgsMapLayer, QgsField, QgsVectorDataProvider
from qgis.gui import QgsMessageBar

import kcluster as kcl


class ClusterAnalysis:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'ClusterAnalysis_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = ClusterAnalysisDialog()

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Cluster Analysis')
        # TODO: We are going to let the user set this up in a future iteration
        self.toolbar = self.iface.addToolBar(u'ClusterAnalysis')
        self.toolbar.setObjectName(u'ClusterAnalysis')

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('ClusterAnalysis', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/ClusterAnalysis/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Cluster Analysis'),
            callback=self.run,
            parent=self.iface.mainWindow())


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Cluster Analysis'),
                action)
            self.iface.removeToolBarIcon(action)
        # remove the toolbar
        del self.toolbar


    def run(self):
        """Run method that performs all the real work"""
        layers = self.iface.legendInterface().layers()
        layer_list = []
        for layer in layers:
            layerType = layer.type()
            #if layerType == QgsMapLayer.VectorLayer:
            layer_list.append(layer.name())
        self.dlg.comboBox.clear()
        self.dlg.comboBox.addItems(layer_list)
        # show the dialog
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result:
            # Do something useful here - delete the line containing pass and
            # substitute with your code.
            selected_layer_index = self.dlg.comboBox.currentIndex()
            kclusters = self.dlg.spinBox.value()
            iterations = self.dlg.spinBox_2.value()

            selected_layer = layers[selected_layer_index]
            selected_extent = selected_layer.extent()
            kcluster_list = []
            for i in range(0, kclusters):
                kx = random.uniform(selected_extent.xMinimum(), selected_extent.xMaximum())
                ky = random.uniform(selected_extent.yMinimum(), selected_extent.yMaximum())
                cluster = kcl.KCluster(kx, ky)
                kcluster_list.append(cluster)
            iteration = 1
            progressMessageBar = self.iface.messageBar().createMessage("Writing to attribute table...")
            progress = QProgressBar()
            progress.setMaximum(kclusters+1)
            progressMessageBar.layout().addWidget(progress)
            self.iface.messageBar().pushWidget(progressMessageBar, self.iface.messageBar().INFO)
            while (iteration <= iterations):
                # Go through every point and find the closest seed
                print("Iteration - {0}".format(iteration))
                features = selected_layer.getFeatures()
                for f in features:
                    cluster = 0
                    min_dist = sys.float_info.max
                    for j in range(0,len(kcluster_list)):
                        dist = kcluster_list[j].Distance(f)
                        if (dist < min_dist):
                            min_dist = dist
                            cluster = j
                    kcluster_list[cluster].AddPoint(f)

                for k in kcluster_list:
                    k.UpdateSeed()
                    if(iteration < iterations ):
                        k.ClearPoints()
                iteration+=1
            caps = selected_layer.dataProvider().capabilities()
            field_name = ''
            if caps & QgsVectorDataProvider.AddAttributes:
                field_name = "cluster0"
                counter = 0
                unsel = True
                index = selected_layer.fields().indexFromName(field_name)
                while index != -1:
                    field_name = field_name[0:-1] + str(counter)
                    index = selected_layer.fields().indexFromName(field_name)
                    counter += 1
                res = selected_layer.dataProvider().addAttributes([QgsField(field_name, QVariant.Int)])
                selected_layer.updateFields()
                index = selected_layer.fields().indexFromName(field_name)
                print('field name: {0}, index: {1}'.format(field_name, index))
                #index = selected_layer.fields().indexFromName(field_name)
                provider = selected_layer.dataProvider()
                for k in range(0, kclusters):
                    progress.setValue(k)
                    kcluster_list[k].PrintSeed()
                    um = kcluster_list[k].AddClusterAttribute(index, k)
                    if len(um) > 0:
                        provider.changeAttributeValues( um )
                self.iface.messageBar().clearWidgets()
                self.iface.messageBar().pushMessage('Info:', "Cluster value added as \"{0}\" in the attribute table".format(field_name), level=QgsMessageBar.INFO)
            else:
                self.iface.messageBar().clearWidgets()
                self.iface.messageBar().pushMessage('ERROR:', 'Unable to update attribute table', level=QgsMessageBar.CRITICAL)

            self.iface.messageBar().pushMessage("Info", "Job Complete", level=QgsMessageBar.INFO, duration=3)
            #progressMessageBar = self.iface.messageBar().createMessage("Doing something boring...")
            #progress = QProgressBar()
            #progress.setMaximum(10)
            #progressMessageBar.layout().addWidget(progress)
            #self.iface.messageBar().pushWidget(progressMessageBar, self.iface.messageBar().INFO)
            #for i in range(10):
            #    time.sleep(1)
            #    progress.setValue(i + 1)
